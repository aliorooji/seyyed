import React from 'react';
import { Route } from 'react-router-dom'
import Home from '../home'
import 'semantic-ui-css/semantic.min.css';

const App = () => (
  <div>
    <main>
      <Route exact path="/" component={Home} />
    </main>
  </div>
)
export default App;

