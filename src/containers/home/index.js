import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Grid, Segment, Icon, Popup} from 'semantic-ui-react'
import SimpleForm from './form';
import {Style} from 'radium';

import {
    addToItems,
    moveTo,
    delIt,
    edit,
} from '../../modules/objs'

const radiumStyles = {
    '.aCenter': {textAlign: 'center'},
    '.vazir': {fontFamily: 'Vazir'},
};
const styles = {
    mainSegments: {height: 400, overflow: 'auto'},
    firstItems: {height: 80, overflow: 'hidden', borderRight: '#76FF03 !important'},
    secondItems: {height: 80, overflow: 'hidden', borderRight: 5 + ' blue'},
    lastItems: {height: 80, overflow: 'hidden', borderRight: 5 + ' red'}
}
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.addToFirst = this.addToFirst.bind(this);
        this.addToSecond = this.addToSecond.bind(this);
        this.addToLast = this.addToLast.bind(this);
        this.state = {
            firstList: [],
            secondeList: [],
            lastList: [],
        };
    }

    setItemsToFather() {
        console.log('setItemsToFather');
        const objs = this.props.objs;
        let firstList = [];
        let secondeList = [];
        let lastList = [];
        if (objs) {
            for (let i = 0; i < objs.length; i++) {
                let obj = objs[i];
                if (obj.father === 'firstList') firstList.push(obj);
                else if (obj.father === 'secondeList') secondeList.push(obj)
                else lastList.push(obj);
            }
        }
        this.setState({
            firstList: firstList,
            secondeList: secondeList,
            lastList: lastList,
        })
    }

    addToFirst(values) {
        const text = values.title;
        this.props.addToItems(text, 'firstList');
        this.setItemsToFather();
    }

    addToSecond(values) {
        const text = values.title;
        this.props.addToItems(text, 'secondeList');
        this.setItemsToFather();
    }

    addToLast(values) {
        const text = values.title;
        this.props.addToItems(text, 'lastList');
        this.setItemsToFather();
    }

    moveToOther(toWhere, id) {
        this.props.moveTo(toWhere, id);
        this.setItemsToFather();
    }

    deleteThis(id) {
        this.props.delIt(id);
        this.setItemsToFather();
    }

    editThis(id, values) {
        this.props.edit(id, values);
        this.setItemsToFather();
    }

    render() {
        const {firstList, secondeList, lastList} = this.state;
        return (
            <div>
                <br/>
                <Grid columns='equal'>
                    <Grid.Column />
                    <Grid.Column width={12}>
                        <Grid>
                            <Grid.Row columns={3} only='computer'>
                                <Grid.Column>
                                    <h3 className="vazir aCenter">ستون اول</h3>
                                    <Segment style={styles.mainSegments}>
                                        {firstList.map(item =>
                                            <Segment className="borderRightGreen" key={'item' + item.id} style={styles.firstItems}>
                                                <Grid columns='equal'>
                                                    <EditDelete
                                                        editHandler={this.editThis.bind(this, item.id)}
                                                        deleteHandler={this.deleteThis.bind(this, item.id)}/>
                                                    <ItemText>{item.text}</ItemText>
                                                    <MoveToOtherBtns
                                                        toLeft={this.moveToOther.bind(this, 'secondeList', item.id)}/>
                                                </Grid>
                                            </Segment>
                                        )}
                                    </Segment>
                                    <SimpleForm submitHandler={this.addToFirst}/>
                                </Grid.Column>
                                <Grid.Column>
                                     <h3 className="vazir aCenter">ستون دوم</h3>
                                    <Segment style={styles.mainSegments}>
                                        {secondeList.map(item =>
                                            <Segment key={'item' + item.id} style={styles.secondItems}>
                                                <Grid columns='equal'>
                                                    <EditDelete
                                                        editHandler={this.editThis.bind(this, item.id)}
                                                        deleteHandler={this.deleteThis.bind(this, item.id)}/>
                                                    <ItemText>{item.text}</ItemText>
                                                    <MoveToOtherBtns
                                                        toLeft={this.moveToOther.bind(this, 'lastList', item.id)}
                                                        toRight={this.moveToOther.bind(this, 'firstList', item.id)}/>
                                                </Grid>
                                            </Segment>
                                        )}
                                    </Segment>
                                    <SimpleForm submitHandler={this.addToSecond}/>
                                </Grid.Column>
                                <Grid.Column>
                                     <h3 className="vazir aCenter">ستون سوم</h3>
                                    <Segment style={styles.mainSegments}>
                                        {lastList.map(item =>
                                            <Segment key={'item' + item.id} style={styles.lastItems}>
                                                <Grid columns='equal'>
                                                    <EditDelete
                                                        editHandler={this.editThis.bind(this, item.id)}
                                                        deleteHandler={this.deleteThis.bind(this, item.id)}/>
                                                    <ItemText>{item.text}</ItemText>
                                                    <MoveToOtherBtns
                                                        toRight={this.moveToOther.bind(this, 'secondeList', item.id)}/>
                                                </Grid>
                                            </Segment>
                                        )}
                                    </Segment>
                                    <SimpleForm submitHandler={this.addToLast}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                    <Grid.Column />
                </Grid>
                <Style rules={radiumStyles}/>
            </div>
        );
    }
}

const EditDelete = props => (
    <Grid.Column>
        <Icon style={{marginBottom: 10}} name='trash'
        onClick={props.deleteHandler}/>
        <br/>
        <Popup
            trigger={<Icon name='edit'/>}
            on='click'
            hideOnScroll
        >
            <Popup.Content>
                <p>لطفا متن مورد نظرتان را وارد نمایید.</p>
                 <SimpleForm submitHandler={props.editHandler}/>
            </Popup.Content>
        </Popup>
    </Grid.Column>
);
const MoveToOtherBtns = props => (
    <Grid.Column>
        {props.toLeft ? <Icon style={{marginBottom: 5}} circular color='teal' name='arrow left'
                              onClick={props.toLeft}/> : ''}
        <br/>
        {props.toRight ? <Icon circular color='teal' name='arrow right'
                               onClick={props.toRight}/> : ''}
    </Grid.Column>
);
const ItemText = props => (
    <Grid.Column width={8}>
        <span>{props.children}</span>
    </Grid.Column>
)
const mapStateToProps = state => ({
    objs: state.objs.objs,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    addToItems,
    moveTo,
    delIt,
    edit,
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

