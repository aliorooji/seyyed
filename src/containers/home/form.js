import React from 'react'
import {Field, reduxForm} from 'redux-form'
import { Button, Input} from 'semantic-ui-react'
import {Style} from 'radium'

const styles = {
    '.margin0': {margin: 0 + ' !important'},
    '.greenLight': { backgroundColor: '#64FFDA'},
}

const renderTextField = ({
  input,
  meta: { touched, error },
    label,
  ...custom
}) => (
  <Input
      className="vazir right"
      placeholder="هر چی دلت میخواد"
    {...input}
    {...custom}
  />
);

const SimpleForm = props => {
    const {handleSubmit, pristine, submitting, submitHandler} = props;
    return (
        <form onSubmit={handleSubmit(submitHandler)}>
                    <div>
                        <div dir="rtl">
                            <Field
                                dir="rtl"
                                name="title"
                                component={renderTextField}
                                type="text"
                            />
                            <Button className='vazir' color="teal" type="submit" disabled={pristine || submitting}>
                            اضافه کن
                        </Button>
                        </div>
                    </div>
            <Style rules={styles}/>
        </form>
    )
}

export default reduxForm({
    form: 'simple' // a unique identifier for this form
})(SimpleForm)