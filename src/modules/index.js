import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import objs from './objs';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  routing: routerReducer,
  objs,
  form: formReducer,
})
