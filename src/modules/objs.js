const ADD_TO_ITEMS = 'ADD_TO_ITEMS';
const DEL = 'DElETE_THIS';
const EDIT = 'EDIT_THIS';
const MOVE = 'MOVE_TO_OTHER';
// const FIND_THIS = 'FIND_THIS';

const initialState = {
    objs: [],
    id: 1
}


export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_ITEMS:
            let obj = {};
            if (action.text) {
                obj = {
                    id: state.id,
                    father: action.forWhere,
                    text: action.text,
                };
            }
            let objs = state.objs;
            objs.push(obj);
            state.id++;
            return {
                id: state.id,
                objs
            };
        case DEL:
            objs = state.objs;
            for (let item of objs) {
                if (item.id === action.id) obj = item;
            }
            let index = objs.indexOf(obj);
            objs.splice(index, 1);
            return {
                id: state.id,
                objs
            };
        case MOVE:
            objs = state.objs;
            for (let item of objs) {
                if (item.id === action.id) item.father = action.toWhere;
            }
            return {
                ...state
            };
        case EDIT:
            objs = state.objs;
            for (let item of objs) {
                if (item.id === action.id) action.values && (item.text = action.values.title);
            }
            return {
                ...state
            };


        default:
            return state
    }
}

export const addToItems = (text, forWhere) => {
    return dispatch => {
        dispatch({type: ADD_TO_ITEMS, text, forWhere});
    }
};
export const delIt = (id) => {
    return dispatch => dispatch({type: DEL, id})
};
export const edit = (id, values) => {
    return dispatch => {
        dispatch({
            type: EDIT,
            id,
            values,
        })
    }
};

export const moveTo = (toWhere, id) => {
    return dispatch => {
        dispatch({
            type: MOVE,
            toWhere,
            id
        })
    }
};
